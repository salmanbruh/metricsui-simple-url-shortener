import random, sqlite3, string
from sqlite3.dbapi2 import Cursor


def sql_connection():
    """
    Initiate connection with in-memory database.
    """

    try:
        connection = sqlite3.connect(':memory:')
        
        return connection
    
    except:
        raise Exception("Cannot connect to sqlite database.")
        


def create_url_table(connection):
    """
    Create table within the in-memory database.
    """

    try:
        cursor = connection.cursor()
    
        query = 'CREATE TABLE url '
        query += '(url text, short_path text PRIMARY KEY, visits integer)'

        cursor.execute(query)

        connection.commit()

        cursor.close()

    except:
        raise Exception("Could not create Table.")


def invalid_command_message():
    """
    Returns the invalid input message.
    """

    message = "{}\n{}\n{}\n{}\n{}\n{}\n{}".format(
        "ERROR: Invalid command. Available commands: ",
        "1. /shorten?url=<url>&short_path=<short-path>",
        "2. /shorten?url=<url>",
        "3. /redirect?url=<pendekin-url>",
        "4. /delete?url=<pendekin-url>",
        "5. /count?url=<pendekin-url>",
        "6. exit"
    )
    
    return message


def get_short_path_from_url(url):
    """
    Returns the short path from the full-url given in parameter.
    """

    short_path = url.split('=')[1].replace('https://pendekin.id/', '')
    return short_path


def generate_short_path():
    """
    Generates short_path consisting of 8 alphanumeric characters. 
    Called if user does not specify the desired short_path in the input.
    """

    length = 8
    short_path = ''

    for i in range(length):
            short_path += random.choice(string.ascii_letters + string.digits)

    return short_path


def insert_url_to_database(connection, url, short_path):
    """
    Insert a url into the database.
    """

    try:
        cursor = connection.cursor()

        db_query = 'INSERT INTO url VALUES '
        db_query += '(?, ?, ?)'

        url_object = (url, short_path, 0)

        cursor.execute(db_query, url_object)

        connection.commit()

        cursor.close()

    except:
        raise Exception("Fail to do database query.")


def delete_url(connection, short_path):
    """
    Deletes a url from the database according to the short path
    specified in the parameter.
    Returns how many rows have been deleted.
    if 0 rows have been deleted, this means that the object
    does not exist ieven before it has been deleted.
    """

    try:
        cursor = connection.cursor()

        db_query = 'DELETE FROM url WHERE short_path="{}"'.format(short_path)

        rows_deleted = cursor.execute(db_query).rowcount

        cursor.close()

        return rows_deleted
    
    except:
        raise Exception("Fail to do database query.")


def get_url_by_short_path(connection, short_path):
    """
    Use database query to fetch the url according to
    the short path specified in the parameter.
    Returns the url in a tuple.
    """

    try:
        cursor = connection.cursor()

        db_query = 'SELECT * FROM url WHERE short_path="{}"'.format(short_path)

        cursor.execute(db_query)

        url_object = cursor.fetchone()

        cursor.close()

        return url_object

    except:
        raise Exception("Fail to do database query.")


def url_is_exist(connection, short_path):
    """
    Checks if the url exists in the database.
    Returns the url if it exists.
    Returns False if the url does not exist.
    """
    url_object = get_url_by_short_path(connection, short_path)

    return url_object if url_object is not None else False


def not_found_message(query):
    """
    Returns a message that indicates that the url specified
    cannot be found in the database.
    """

    short_path = get_short_path_from_url(query)
    return "Error: {} could not be found in the service.".format(short_path)


def add_visits(connection, short_path):
    """
    Increments a url's visit counter by 1.
    Called when the input is /redirect.
    """

    try:
        cursor = connection.cursor()

        db_query = 'UPDATE url SET visits = visits + 1 WHERE short_path="{}"'
        db_query = db_query.format(short_path)

        cursor.execute(db_query)

        cursor.close()

    except:
        raise Exception("Fail to do database query.")  


def shorten(connection, query):
    """
    Shortens the url specified in the query by replacing it with
    https://pendekin.id/<short_path> where short_path is specified
    in the query parameter. If the short path is not specified, 
    then the service will generate a short path automatically.
    Adds the shortened url to the database and returns the shortened url.
    """

    query_split = query.split('&')

    url = None
    short_path = None

    for sub_query in query_split:
        sub_query_split = sub_query.split('=')

        query_type = sub_query_split[0]
        query_value = sub_query_split[1]

        if query_type == 'url':
            url = query_value
        elif query_type == 'short_path' or query_type == 'desired':
            short_path = query_value

    if short_path is None:
        short_path = generate_short_path()

    insert_url_to_database(connection, url, short_path)

    output_str = "https://pendekin.id/{}".format(short_path)

    return output_str


def redirect(connection, query):
    """
    Increments the shortened url visit counter by 1, then 
    returns the original url of the shortened url specified in the
    query parameter. Will return a not found message if the 
    shortened url specified in the query parameter does not exist
    in the database.
    """

    short_path = get_short_path_from_url(query)
    url_object = url_is_exist(connection, short_path)

    if not url_object:
        return not_found_message(query)

    add_visits(connection, short_path)

    redirect_url = url_object[0]

    return redirect_url


def delete(connection, query):
    """
    Deletes the shortened url from the database.
    If the number of rows deleted is 1, it will return a
    Sucess string. Else if the number of rows deleted is 0,
    then it will return not found message as the query have
    deleted 0 rows.
    """

    short_path = get_short_path_from_url(query)
    rows_deleted = delete_url(connection, short_path)

    if rows_deleted == 1:
        return "Success"
    else:
        return not_found_message(query)


def count(connection, query):
    """
    Checks if the url exists in database.
    If it does, then return the amount of visits.
    Else, return a not found message.
    """

    short_path = get_short_path_from_url(query)
    url_object = url_is_exist(connection, short_path)

    if not url_object:
        return not_found_message(query)

    url_count = url_object[2]

    return url_count


def input_is_valid(input_string):
    split_input = input_string.split('?')

    if len(split_input) < 2:
        return False


def main():
    """
    Main function.
    Makes a connection with in-memory database.
    Then, creates the url table within the database.
    Then, takes user input, calls a function based on the input,
    and prints the output of the function call.
    """
    connection = sql_connection()

    create_url_table(connection)

    while True:
        user_input = input('>> ')

        if user_input == 'exit': break

        if not input_is_valid(user_input):
            print("No query strings have been given.")
            print()
            continue

        split_input = user_input.split('?')

        path = split_input[0]
        query = split_input[1]
        output = ''

        if path == '/shorten':
            output = shorten(connection, query)
        elif path == '/redirect':
            output = redirect(connection, query)
        elif path == '/delete':
            output = delete(connection, query)
        elif path == '/count':
            output = count(connection, query)
        else:
            output = invalid_command_message()

        print(output)
        print()

    connection.close()


if __name__ == '__main__':
    main()