# Simple and CLI-based URL Shortener

Simple CLI-based URL Shortener using SQLite as in-memory database.



## How to use

Run the program:

```
python pendekin.py
```



## Available commands:

- Shortening a URL : returns the shortened URL.

  ```
  # Syntax
  >> /shorten?url=<url-to-be-shortened>&short_path=<short-path>
  or
  >> /shorten?url=<url-to-be-shortened>
  
  # Example
  >> /shorten?url=https://www.some-url.com/&short_path=some-path
  or
  >> /shorten?url=https://www.some-url.com/ 
  ```

  

- Redirecting a URL : returns the original URL.

  ```
  # Syntax
  >> /redirect?url=https://pendekin.id/<short-path>
  
  # Example
  >> /redirect?url=https://pendekin.id/some-path
  ```

  

- Delete a shortened URL : returns Success

  ```
  # Syntax
  >> /delete?url=https://pendekin.id/<short-path>
  
  # Example
  >> /delete?url=https://pendekin.id/some-path
  ```

  

- Check how many times a URL has been viewed.

  ```
  # Syntax
  >> /count?url=https://pendekin.id/<short-path>
  
  # Example
  >> /count?url=https://pendekin.id/some-path
  ```

  

- Exit

  ```
  # Syntax
  >> exit
  ```

  



## Example

```
<file-dir>$ python pendekin.py
>> /redirect?url=https://pendekin.id/scele 
Error: scele could not be found in the service.

>> /delete?url=https://pendekin.id/scele   
Error: scele could not be found in the service.

>> /count?url=https://pendekin.id/scele  
Error: scele could not be found in the service.

>> /shorten?url=https://scele.cs.ui.ac.id/&short_path=scele
https://pendekin.id/scele

>> /redirect?url=https://pendekin.id/scele
https://scele.cs.ui.ac.id/

>> /redirect?url=https://pendekin.id/scele
https://scele.cs.ui.ac.id/

>> /count?url=https://pendekin.id/scele
2

>> /delete?url=https://pendekin.id/scele
Success

>> /shorten?url=https://scele.cs.ui.ac.id/                 
https://pendekin.id/Nwknj7To

>> /redirect?url=https://pendekin.id/Nwknj7To
https://scele.cs.ui.ac.id/

>> /redirect?url=https://pendekin.id/Nwknj7To
https://scele.cs.ui.ac.id/

>> /redirect?url=https://pendekin.id/Nwknj7To
https://scele.cs.ui.ac.id/

>> /count?url=https://pendekin.id/Nwknj7To
3

>> /delete?url=https://pendekin.id/Nwknj7To
Success

>> exit

```

